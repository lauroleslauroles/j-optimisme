<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/7b7a0d17fe.js"></script>
    <title>Joptimisme</title>
  </head>
  <body>
    <header>
      <nav class="navbar navbar-dark bg-primary navbar-expand-lg">
            <div class="container">
                <p class="row col-12">
                        <a id="home" class="navbar-brand" href="#"><i class="fas fa-laptop fa-3x"></i></a>
                        <p class="text-center">J'optimisme</p>
            </div>
        </nav>
    </header>
    <div class="container">
        <ul class="list-group">

            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5">
                <a href="#" data-toggle="modal" data-target="#processorModal">
                    + Processeur
                    <span class="badge badge-primary badge-pill">14</span>
                </a>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5">
                <a href="#" data-toggle="modal" data-target="#cartegraphiqueModal">
                    + Carte graphique
                    <span class="badge badge-primary badge-pill">14</span>
                </a>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5">
                <a href="#" data-toggle="modal" data-target="#stockageModal">
                    + Stockage
                    <span class="badge badge-primary badge-pill">14</span>
                </a>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5">
                <a href="#" data-toggle="modal" data-target="#alimentationModal">
                    + Alimentation
                    <span class="badge badge-primary badge-pill">14</span>
                </a>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5">
                <a href="#" data-toggle="modal" data-target="#boitierModal">
                    + Boitier
                    <span class="badge badge-primary badge-pill">14</span>
                </a>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5">
                <a href="#" data-toggle="modal" data-target="#ramModal">
                    + Ram
                    <span class="badge badge-primary badge-pill">14</span>
                </a>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5">
                <a href="#" data-toggle="modal" data-target="#ventiradModal">
                    + Ventirad
                    <span class="badge badge-primary badge-pill">14</span>
                </a>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5">
                <a href="#" data-toggle="modal" data-target="#cartemereModal">
                    + Carte mère
                    <span class="badge badge-primary badge-pill">14</span>
                </a>
            </li>


        <!-- Modal Processor -->
        <div class="modal fade" id="processorModal" tabindex="-1" role="dialog" aria-labelledby="processorModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <h5>Processeur 1</h5>
                            <p>Nom processeur 1.</p>
                            <hr>
                            <h5>Processeur 2</h5>
                            <p>Nom processeur 2.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Ventirad -->
        <div class="modal fade" id="ventiradModal" tabindex="-1" role="dialog" aria-labelledby="ventiradModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ventiradModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <h5>Ventirad 1</h5>
                            <p>Ventirad 1.</p>
                            <hr>
                            <h5>Ventirad 2</h5>
                            <p>Ventirad 2.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Ram -->
        <div class="modal fade" id="ramModal" tabindex="-1" role="dialog" aria-labelledby="ramModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ramModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <h5>Ram 1</h5>
                            <p>Ram 1.</p>
                            <hr>
                            <h5>Ram 2</h5>
                            <p>Ram 2.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Boitier -->
        <div class="modal fade" id="boitierModal" tabindex="-1" role="dialog" aria-labelledby="boitierModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="boitierModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <h5>Boitier 1</h5>
                            <p>Boitier 1.</p>
                            <hr>
                            <h5>Boitier 2</h5>
                            <p>Boitier 2.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Carte graphique -->
        <div class="modal fade" id="cartegraphiqueModal" tabindex="-1" role="dialog" aria-labelledby="cartegraphiqueModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="cartegraphiqueModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <h5>Carte graphique</h5>
                            <p>Carte graphique</p>
                            <hr>
                            <h5>Carte graphique</h5>
                            <p>Carte graphique</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Carte mère -->
        <div class="modal fade" id="cartemereModal" tabindex="-1" role="dialog" aria-labelledby="cartemereModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="cartemereModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <h5>Carte mère 1</h5>
                            <p>Carte mère 1.</p>
                            <hr>
                            <h5>Carte mère 2</h5>
                            <p>Carte mère 2.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Alimentation -->
        <div class="modal fade" id="alimentationModal" tabindex="-1" role="dialog" aria-labelledby="alimentationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="alimentationModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <h5>Alimentation 1</h5>
                            <p>Alimentation 1.</p>
                            <hr>
                            <h5>Alimentation 2</h5>
                            <p>Alimentation 2.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Stockage -->
        <div class="modal fade" id="stockageModal" tabindex="-1" role="dialog" aria-labelledby="stockageModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="stockageModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <h5>Stockage 1</h5>
                            <p>Stockage 1.</p>
                            <hr>
                            <h5>Stockage 2</h5>
                            <p>Stockage 2.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

    </div>
    <footer>
    </footer>
  </body>
</html>
