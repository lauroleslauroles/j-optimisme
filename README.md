Projet A - Magasin de vente informatique en ligne
Pitch du projet
Vous allez réaliser la plateforme de vente en ligne d'une entreprise vendant des composants informatique. Cette boutique ressemble à beaucoup d'autres et souhaite se positionner sur le segment des sites spécialisés (GrosBill, Materiel.net, Hardware.fr, LDLC, etc.) en proposant une offre exclusivement focalisée sur les composants informatique, tout en offrant un accompagnement à l'utilsiateur dans son achat : suggestions de composants en lien avec ce qu'il regarde, etc.

Considérant ce second point, il va être nécessaire d'agréger les informations de navigation, d'achats et d'avis des utilisateurs afin de proposer aux utilisateurs suivants des recommandations pertinentes.

Sur ce projet, vous réaliserez tout le travail d'étude et de modélisation de la base de données, les stratégies & scripts de déploiements et de sauvegardes, ainsi qu'un prototype attestant de la faisabilité technique de votre proposition.

Fonctionnalités attendues
Gestion des composants
Il doit être possible pour un utilisateur authentifié et de profil gestionnaire de pouvoir créer/modifier/supprimer des fiches produit. Vous déterminerez les informations pertinentes mais les produits auront des caractéristiques communes (type de composant, prix, poids, durée de garantie, etc.) et des propriétés spécifiques à chaque type (fréquence CPU, GPU intégré, connectiques sur carte mère, capacité de mémoire RAM/GRAM, etc...).

Les caractéristiques évoluant fréquemment sont le stock et le prix. Les autres informations ne devraient être modifiées qu'en cas d'erreur.

Compatibilité des composants
Une carte mère est compatible avec certains types de processeurs, avec certains types de RAM, pour une capacité et une fréquence de RAM maximum, etc.

En résumé, tous les composants ne sont pas faits pour fonctionner ensemble. Il faut donc pouvoir rechercher pour un composant donné l'ensemble des composants compatibles. Vous pouvez gérer cela de plusieurs manières techniquement, à vous d'estimer la manière la plus pertinente dans le laps de temps du projet.

Recherche de composants
Un utilisateur doit pouvoir rechercher des composants selon de nombreux critères usuels (prix, type de composant, capacité si disque dur, etc.). Vous devrez donc établir une liste de critères communs ET une liste de critères spécifique à chaque type de composants afin de permettre à l'utilisateur de rechercher sur un ou plusieurs de ces critères.

Assistant d'assemblage de PC
Notre site proposera une petite fonctionnalité novatrice dans la forme (même si dans le fond, ce genre de choses existent) : sachant qu'un ordinateur est composé d'une carte mère, un ou plusieurs processeurs, au moins une barette de RAM, au moins une carte graphique (sauf si le processeur embarque un GPU et que la carte mère offre des connectiques graphiques), d'au moins un HDD ou SSD, etc. vous proposerez une petit outil permettant en partant d'un panier utilisateur d'estimer les composants manquants, de masquer les types de composants qui ne sont plus nécesaires (si j'ai mis 4 barettes de RAM et que la carte mère n'a que 4 slots, plus de RAM !). Pourquoi pas proposer cela de manière visuelle ?

Gestion des paniers/commandes
Il faut tracer les paniers, les commandes et leurs avancement, etc. Il faudra donc tracer toutes ces choses dans une ou plusieurs collections.

Recommandations
Se basant sur toutes les informations tracées dans la fonctionnalité précédente, il va falloir proposer plusieurs services :

Pour un composant donné, suggérer les autres composants fréquemment achetés avec.
Recommander les 10 composants les plus achetés les dernieères semaines
Autres recommandations que vous jugerez sympatoches !
Ce calcul se met à jour toutes les nuits à 2h.